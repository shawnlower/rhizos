# Item Types

## Event
## Webpage
## Todo Item
## Goal
## Photo
## Other file

# UX/Interaction

Start from:
    Event:
        { type: Event, title: "Governor's Ball", Date: 2020-12-12,
          rels: [{ type: RELATED_TO, target: .., notes: "" }] }
    Email:
        { type: Email, title: "Re: your appt", Received: 2020-12-22,
        }
    WebPage:
        { type: URL, title: "Title of the URL", url: http://example.com/ }
    Goal:
        { type: Goal, title: "World Domination" }
    Todo:
        { type: Todo, title: "Clean the house", dueDate: .., frequency: .. }
    Photo:
        { type: Photo, title: "Picture of Lago de Atitlan" }

? Every item has a set of properties, and a list of serializations. e.g.
    { format: image/jpeg, url: "http://.../foo.jpg }
    { format: text/html, url: "http://.../archive-2020-11-18.html }

# Methods

## Method One: in code


## Method Two: dynamic (in DB)

- Base static properties
    - Item: name, type, url
    - Type: name, description, url, properties
        - Property: name, description, url

## Hybrid

- Use base 'Item' type
- Create custom type, e.g. Note
- Note has create() method that takes an Item (or separate fromDTO() / fromItem() methods)
