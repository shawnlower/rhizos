import path from 'path';
import { Configuration, HotModuleReplacementPlugin } from 'webpack';
import ManifestPlugin from 'webpack-manifest-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';

import { SERVER_PORT, IS_DEV, WEBPACK_PORT } from './src/api/config';

const BUILD_TEMPLATE = IS_DEV ? './src/ui/index.html' : './src/ui/index.html';

const plugins = [
  new ManifestPlugin(),
  new HtmlWebpackPlugin({
    hash: true,
    title: 'Rhizos!',
    myPageHeader: 'Rhizos!',
    template: './src/ui/index.html',
    filename: 'index.html' //relative to root of the application
  }),
  new HotModuleReplacementPlugin()
];

const nodeModulesPath = path.resolve(__dirname, 'node_modules');

const config: Configuration = {
  mode: IS_DEV ? 'development' : 'production',
  devtool: IS_DEV ? 'inline-source-map' : false,
  entry: {
    main: "./src/ui/App",
    shared: ["core-js", "lodash"],
  },
  output: {
    path: path.join(__dirname, 'dist', 'statics'),
    filename: `js/[name]-[hash:8]-bundle.js`,
    publicPath: IS_DEV ? '/' : './',
  },
  resolve: {
    extensions: ['.js', '.ts', '.tsx'],
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          chunks: 'all',
        },
      },
    },
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: [/node_modules/, nodeModulesPath],
        use: {
          loader: 'babel-loader',
          options: {
          },
        },
      },
      {
        test: /.jpe?g$|.gif$|.png$|.svg$|.woff$|.woff2$|.ttf$|.eot$/,
        use: {
          loader: 'url-loader',
          options: {
            name: "img/[name].[ext]",
            limit: 10000
          },
        },
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
  devServer: {
    port: WEBPACK_PORT,
    hot: true,
    open: IS_DEV,
    openPage: `http://localhost:${WEBPACK_PORT}`,
    historyApiFallback: true,
    proxy: {
          '/api': 'http://localhost:5000'
    }
  },
  plugins,
};

export default config;
