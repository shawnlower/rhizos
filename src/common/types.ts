import { IItemType } from "./models";

export const T_Note: IItemType = {
    name: "Note",
    description: "A small text note.",
    id: "Note",
    properties: [
        { id: "text", name: "Text", datatype: "text" }
    ],
}

export const T_WebPage: IItemType = {
    name: "WebPage",
    description: "A single page on a website",
    id: "WebPage",
    properties: [
        { id: "title", name: "Page Title", datatype: "string" },
        { id: "url", name: "URL", datatype: "string" },
        { id: "author", name: "Author", datatype: "string" }
    ], // title, author, etc    
}

export const T_Task: IItemType = {
    name: "Task",
    description: "A task that needs to be done",
    id: "Task",
    properties: [
        { id: "completed", name: "Completed", datatype: "boolean" },
        { id: "dateCompleted", name: "Date Completed", datatype: "date" }
    ], // title, author, etc
}