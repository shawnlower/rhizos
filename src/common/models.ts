import { string } from "prop-types";
import _ from 'lodash';

export interface IItemDTO {
    name: string;
    item_type: string;
    // date_created: Date;
    item_id: string | null;
    properties: any;
}

export interface IItemModel {
    title: string;
    itemTypeId: string;
    dateCreated: Date | null;
    id: string | null;
    properties: any;
}

export interface IItemTypeDTO {
    name: string; // WebPage, Document, Note, Image
    type_id: string;  // e.g. NoteDigitalDocument
    description?: string;
    properties?: IItemPropertyDTO[]; // author, url, etc
    icon?: string;
    // Renderers, etc?
}
export interface IItemTypeModel {
    name: string; // WebPage, Document, Note, Image
    id: string;  // e.g. NoteDigitalDocument
    description: string;
    properties: IItemPropertyModel[]; // author, url, etc
    icon?: string;
    // Renderers, etc?
}

export interface IItemPropertyDTO {
    name: string; // e.g. author, url, etc
    property_id: string; // e.g. author
    description?: string;
    // datatype: string; // change to enum (boolean, string, number, text, date)
    property_range: any;
    value?: any,
}
export interface IItemPropertyModel {
    name: string; // e.g. author, url, etc
    id: string; // e.g. author
    datatype: string; // change to enum (boolean, string, number, text, date)
    value?: any,
}


export class ItemProperty implements IItemPropertyModel {
    constructor(
        public name: string,
        public readonly id: string,
        public datatype: string,
        private _value: any = null,
        private api?: any,
    ) {

        this.name = name;
        this.datatype = datatype;
        this.id = id;
        this._value = _value;
        this.api = api;
    }
    static create(dto: IItemPropertyDTO) {
        /* Create a new class from a DTO */

        // For now, we'll just grab the first property_range
        // TODO: this will need more work to accomodate rels
        if (!dto.property_range) console.warn("No range specified for ", dto)
        const datatype  = dto.property_range ? dto.property_range[0] : "string";
        const prop = new ItemProperty(
            dto.name,
            dto.property_id,
            datatype,
            dto.value || null,
        )
        return prop;
        
    }

    public toDto(): IItemPropertyDTO {
        const dto: IItemPropertyDTO = {
            name: this.name,
            property_id: this.id,
            property_range: [ this.datatype ],
            value: this.value,
        }
        // ensure value is serializable
        console.log('serializing to DTO', dto)
        return dto;
    }

    public get value(): string {
        if (this._value == null) return "";
        switch(this.datatype) {
            case "date": {
                return new Date(this._value).toLocaleDateString()
            }
            default: {
                return this._value.toString();
            }
        }
    }
    public set value(v) {
        switch(this.datatype) {
            case "date": {
                this._value = new Date(v);
            }
            default: {
                this._value = v;
            }
        }
    }


}

export class ItemType implements IItemTypeModel {

    constructor(
        public name: string,
        public description: string = "",
        public readonly id: string,
        public properties: ItemProperty[],
        public icon?: string,
    ) {
        this.name = name;
        this.description = description;
        this.properties = properties;
        this.id = id;
        this.icon = icon;
    }

    static create(dto: IItemTypeDTO) {
        /* Create a new class from a DTO */
        const properties = dto.properties ? dto.properties : [];
        const itemType = new ItemType(
            dto.name,
            dto.description,
            dto.type_id,
            properties.map(
                 propDTO => ItemProperty.create(propDTO)),
            dto.icon || "",
        );
        return itemType;
    }
}

export class Item implements IItemModel {
    constructor(
        public title: string,
        public itemTypeId: string,
        public dateCreated: Date | null,
        public id: string|null,
        public properties: any,
        private api?: any,
    ){
        this.title = title;
        this.itemTypeId = itemTypeId;
        this.dateCreated = dateCreated;
        this.id = id;
        this.api = api;
    }

    getItemType(): Promise<ItemType> {
        if (!this.api) {
            throw new Error("Unable to lookup item type without a connected API")
        }
        // console.log('getItemType: ', this.api, this.api.getTypeById(this.itemTypeId));
        return this.api.getTypeById(this.itemTypeId);
    }

    static create(dto: IItemDTO, api: any = null) {
        /* Create a new class from a DTO */
        const properties = dto.properties ? dto.properties : [];

        // Set the date if present
        const dtoDate = _.find(properties, {name: 'created'});

        let createdDate: Date | null = null;
        if (dtoDate) {
            createdDate = new Date(dtoDate.value);
        } else {
            createdDate = null;
        }

        return new Item(
            dto.name,
            dto.item_type,
            createdDate,
            dto.item_id,
            properties.map(
                (propDTO: IItemPropertyDTO) => ItemProperty.create(propDTO)),
            api,
        )
    }

    public toDto(): IItemDTO {
        console.log('serializing item');
        const dto: IItemDTO = {
            name: this.title,
            item_type: this.itemTypeId,
            item_id: this.id,
            properties: this.properties.map((prop: ItemProperty) => prop.toDto()),
        }
        return dto;
    }

    public async addProperty(propertyId: string, value: any) {
        /* Add a property, calling the API to complete if defined */
        if (this.api) {
            const myType = await this.api.getTypeById(this.itemTypeId);
            const prop = _.find(myType.properties, { id: propertyId });
            const ns = this.api.config.namespace;

            // If our value is a literal, set it. Otherwise, we use the ID
            // of the object
            switch (prop.datatype) {
                case 'string':
                    prop.value = value
                    break;

                default:
                    prop.value = `${ns}/${value.id}`;
            }
            console.log('addProperty', myType, prop, value)
            this.api.addItemProperty(this, propertyId, prop.value);
        }
    }

    public deleteProperty(propertyId: string) {
        /* Delete a property, calling the API to complete if defined */
        if (this.api) {
            this.api.deleteItemProperty(this, propertyId);
        }
    }
}

// export interface IRelation {
//     relationType: string;
// }

// export class WebPage extends Item {
//     constructor() {
//         super()
//     }
// }


/*********
 * Search
 ********/

export interface ItemSearchResult {
    id: string;
    icon: string;
    title: string;
    type: string;
    dateCreated: Date;
    url: string;
}


 