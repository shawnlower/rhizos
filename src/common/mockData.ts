import faker from 'faker'
import _ from 'lodash';

import { ItemSearchResult, Item, ItemProperty, IItemDTO, IItemTypeDTO, IItemPropertyDTO } from "../common/models";

const getPropsById = (props: string[]) => props.map(prop => mockProperties[prop]);

export const mockProperties: {[key:string]: IItemPropertyDTO} = {
    author: {
        id: "author",
        name: "Author",
        datatype: "string",
    },
    isbn: {
        id: "isbn",
        name: "ISBN Number",
        datatype: "string",
    },
    numPages: {
        id: "numPages",
        name: "Number of Pages",
        datatype: "number",
    },
    publicationDate: {
        id: "publicationDate",
        name: "Date of publication",
        datatype: "date",
    },
}
const createProp = (id: string, value: any) => {
    const ip: any = ItemProperty.create(mockProperties[id]);
    ip.value = value;
    return ip;
}

export const mockTypes: IItemTypeDTO[] = [
    {
        id: "Note",
        icon: "sticky note outline",
        name: "Note",
        description: "A text note",
        properties: [
 
        ],
    },
    {
        id: "WebPage",
        icon: "firefox",
        name: "WebPage",
        description: "A Web Page",
        properties: [

        ],
    },
    {
        id: "Book",
        icon: "book",
        name: "Book",
        description: "A Book; physical or digital",
        properties: getPropsById(
                ["author", "publicationDate", "numPages"]
            ),
    },
    {
        id: "Task",
        icon: "tasks",
        name: "Task",
        description: "A task to be completed",
        properties: [

        ],
    },
    {
        id: "Event",
        icon: "calendar alternate outline",
        name: "Event",
        description: "An event to occur on a specific date",
        properties: [

        ],
    },
];

export const mockItems: IItemDTO[] = [
    {
        id: "seveneves",
        title: "SevenEves",
        // type: _.find(mockTypes, {id: "WebPage"}),
        item_type: "Book",
        date_created: faker.date.past(),
        properties: [
            createProp("numPages", "880"),
            createProp("author", "Neal Stephenson"),
            createProp("publicationDate", "2005-05-19"),
        ],
        // url: `/items/habitize`,
    },
    {
        id: "reading-list",
        title: "Reading List",
        // type: _.find(mockTypes, {id: "WebPage"}),
        item_type: "Note",
        date_created: faker.date.past(),
        properties: [],
        // url: `/items/habitize`,
    },
    {
        id: "untitled-note",
        title: "An Untitled Note",
        item_type: "Note",
        date_created: faker.date.past(),
        properties: [],
    },
    {
        id: "island-of-vice",
        title: "Island of Vice: Atlas Obscura",
        item_type: "WebPage",
        date_created: faker.date.past(),
        properties: [],
    },
    {
        id: "buy-new-shoes",
        title: "Buy new shoes",
        item_type: "Task",
        date_created: faker.date.past(),
        properties: [],
    },
    {
        id: "on-natural-language",
        title: "On Natural Language",
        // type: _.find(mockTypes, {id: "WebPage"}),
        item_type: "Book",
        date_created: faker.date.past(),
        properties: [
            // author: "Noam Chomsky"
        ],
        // url: `/items/habitize`,
    },
    {
        id: "designing-data-intensive-applications",
        title: "Designing Data-intensive Applications",
        // type: _.find(mockTypes, {id: "WebPage"}),
        item_type: "Book",
        date_created: faker.date.past(),
        properties: [],
        // url: `/items/habitize`,
    },
]


// const fakeSource: ItemSearchResult[] = _.times(15, (idx: number) => ({
//     id: idx,
//     icon: faker.image.cats(),
//     title: faker.lorem.sentence(),
//     type: faker.commerce.product(),
//     dateCreated: faker.date.past(),
//     url: `/items/${idx}`,
// }))