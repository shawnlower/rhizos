import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import { Container } from 'semantic-ui-react';
import { compose, createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import { loggerMiddleware, asyncDispatchMiddleware } from './redux/middleware';

import rootReducer from './redux/reducers';
import Items from './pages/Items';
import TypeList from './pages/TypeList';
import ItemDetail from './pages/ItemDetail';
import TypeDetail from './pages/TypeDetail';
import NavBar from './components/NavBar';
import { LoginPage } from './pages/Login';

export const App = () => (
  <>
    <NavBar />
    <Container style={{ marginTop: '5em' }}>
      <Switch>
        <Route path='/items/:itemId' component={ItemDetail} />
        <Route path='/types/:typeId' component={TypeDetail} />
        <Route exact path='/login' component={LoginPage} />
        <Route exact path='/items' component={Items} />
        <Route exact path='/types' component={TypeList} />
        <Route exact path='/'>
          <Redirect to="/items"></Redirect>
        </Route>
      </Switch>
    </Container>
  </>
)

const ENABLED_MIDDLEWARE = [ asyncDispatchMiddleware ];
// const ENABLED_MIDDLEWARE = [loggerMiddleware, asyncDispatchMiddleware];

/* Enable Redux dev-tools */
const composeEnhancers =
  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
    }) : compose;

const enhancer = composeEnhancers(
  applyMiddleware(...ENABLED_MIDDLEWARE),
);

/*
 * Create Redux store
 */
const store = createStore(rootReducer, {}, enhancer);

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>, document.getElementById('app'));
