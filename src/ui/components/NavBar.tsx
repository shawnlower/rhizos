import React from 'react';
import { Dropdown, Image, Menu } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import logo from '../../static/logo.png';

export class NavBar extends React.Component<any> {

  public render() {

    return (
      <Menu fixed='top' inverted>
        <Menu.Item header>
          <Image size='mini' src={logo} style={{ marginRight: '1.5em' }} />
          <Link to="/">Home</Link>
        </Menu.Item>
        <Menu.Item header>
          <Link to="/items">Items</Link>
        </Menu.Item>
        <Menu.Item header>
          <Link to="/types">Types</Link>
        </Menu.Item>
        <Menu.Item position="right">
          <Dropdown
              button
              labeled
              className='icon'
              icon='world'
              text={this.props.user.name}
          >
            <Dropdown.Menu
            >
              <Dropdown.Item>
                <Link to="/login" className="ui">
                <span style={{"display": "block"}}>
                  Log In
                </span>
                </Link>
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </Menu.Item>
      </Menu>
    )
  }

  public componentDidMount() {
  }
    
}

const mapStateToProps = (state: any, ownProps: any) => {
  return {
    ...state,
  }
}

const mapDispatchToProps = dispatch => {
   return {
    //  completeTaskInstance: (taskInstance: TaskInstance) => dispatch({type: 'COMPLETE_TASK_INSTANCE', taskInstance}),
   }
}
export default connect(mapStateToProps, mapDispatchToProps)(NavBar);