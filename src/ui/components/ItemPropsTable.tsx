import _ from 'lodash';
import { Message, Container, Table, Input, Search, Button } from "semantic-ui-react";
import * as React from 'react';

import { Item, ItemProperty } from "../../common/models";
import InputItemCompleter from "./InputItemCompleter";
import InputFlexibox from './InputFlexibox';
import itemDetail from '../redux/reducers/itemDetail';
import { connect } from "react-redux"


export interface ItemPropsTableProps {
    item: Item;
    refreshItem: any;    
    visibleProperties?: ItemProperty[]; // to pass in initial set of properties to render
}

export interface ItemPropsTableState {
    visibleProperties: ItemProperty[];
    allProperties: ItemProperty[]; // for search

    newProperty: ItemProperty | null; // The property object.
    newPropertyValue: Item | string | null; // The value itself

    newPropertyInput: string; // input box for new property
    newPropertySearchResults: SearchResult[] // filtered results for new property box

    isLoading: boolean;
}

interface SearchResult { title: string, description: string};

class ItemPropsTable extends React.Component<ItemPropsTableProps, ItemPropsTableState> {
    initialState = {
        visibleProperties: [],
        allProperties: [],
        isLoading: true,
        newPropertyInput: "",
        newProperty: null,
        newPropertySearchResults: [],
    }

    constructor(props: ItemPropsTableProps) {
        super(props);
        console.log("Got props", props);
        this.state = { ...this.initialState }

        this.saveProperty = this.saveProperty.bind(this);
        this.clearPropertyInputs = this.clearPropertyInputs.bind(this);
        this.setValueForProperty = this.setValueForProperty.bind(this);
    }

    componentDidMount() {
        this.setState({ visibleProperties: this.props.item.properties})
        this.getProperties().then(properties => {
            this.setState({
                allProperties: properties,
                isLoading: false,
            })
            console.log('Loaded properties', properties)
        })
    }

    clearPropertyInputs(e, v) {
        // console.log('clearPropertyInputs called with', e, v)
        this.setState({
            newProperty: null,
            newPropertyInput: "",
        })
    }

    saveProperty() {
        console.log('saveProperty called.', this.state.newProperty)
        const newProp = this.state.newProperty;
        const value = this.state.newPropertyValue;
        if (!newProp) {
            console.error("No property to save!")
        }

        this.props.item.addProperty(newProp.id, value).then(() => {
            // Delay for server write to complete
            setTimeout(() => {
                this.props.refreshItem(this.props.item.id)
            }, 100);
        });

    }

    setValueForProperty(v: any) {
        const newProp = this.state.newProperty;
        if (!newProp) {
            console.warn("newProperty is unexpectedly null/undefined!");
            return;
        }

        console.log("Got value", v)
        this.setState({ newProperty: newProp });
        this.setState({ newPropertyValue: v });
    }

    async getProperties(): Promise<ItemProperty[]> {
        return this.props.item.getItemType()
            .then(itemType => itemType.properties)
    }

    handlePropertySelect = (e, { result }) => {
        /*
        When we confirm our new property, we want to update the input both,
        as well as set the property object in our local state. This will allow
        the 'value' input to be rendered.
        */

        const newProp = _.find(this.state.allProperties, { name: result.title }) || null;

        this.setState({
            newPropertyInput: result.title,
            newProperty: newProp,
        })
    }

    handlePropertySearchChange = (e, { value }) => {
        const initialState = { isLoading: false, newPropertyInput: "" };
        this.setState({ isLoading: true, newPropertyInput: value })

        // All properties that aren't current visible (already on our item)
        // Use v.id as the criterion for generating the set difference
        // const allProps: ItemProperty[] = _.differenceBy(
        //     this.state.allProperties, this.state.visibleProperties, v => v.id);

        // Duplicate properties should be allowed
        const allProps: ItemProperty[] = this.state.allProperties;

        const source: SearchResult[] | null = allProps.map(prop => ({
            title: prop.name,
            description: prop.name,
        }));

        setTimeout(() => {
            if (this.state.newPropertyInput.length < 1) return this.setState(initialState)

            const re = new RegExp(_.escapeRegExp(this.state.newPropertyInput), 'i')
            const isMatch = (result: SearchResult) => re.test(result.title)

            this.setState({
                isLoading: false,
                newPropertySearchResults: _.filter(source, isMatch),
            })
        }, 300)
    }

    deleteProperty(prop: ItemProperty) {
        console.log('Deleting property', prop, this.props.item)
        this.props.item.deleteProperty(prop.id);
        this.props.refreshItem(this.props.item.id);
    }

    render() {
        if (!this.props.item) {
            return <Message>No item passed to table :-(</Message>
        } else {
            const itemName = this.props.item.title;
            let propsLabel: any;
            let renderProps: ItemProperty[] = []; 

            for (const prop of this.props.item.properties) {
                if (renderProps.filter(p => p.id === prop.id).length > 0) {
                    console.warn("Duplicate property ID detected", prop);
                }
                renderProps.push(prop);
            }
            propsLabel = <Message>Rendering all {renderProps.length.toString()} properties from {itemName}</Message>
            return (
                <Container>
                    {propsLabel}
                <Table>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>Property Type</Table.HeaderCell>
                                <Table.HeaderCell>Value</Table.HeaderCell>
                                <Table.HeaderCell>Type</Table.HeaderCell>
                                <Table.HeaderCell
                                    style={{ width: "50px" }}
                                >Delete</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <Table.Body>
                        {
                        renderProps.map(prop =>
                            (<Table.Row key={prop.id}>
                                <Table.Cell>{prop.name}</Table.Cell>
                                <Table.Cell>{prop.value}</Table.Cell>
                                <Table.Cell>{prop.datatype}</Table.Cell>
                                <Table.Cell>
                                    <Button
                                    onClick={() => this.deleteProperty(prop)}
                                    >Delete</Button>
                                </Table.Cell>
                            </Table.Row>)
                        )
                        }
                        {
                            <Table.Row key="__empty_row">
                                <Table.Cell>
                                    <Search
                                        loading={this.state.isLoading}
                                        onResultSelect={this.handlePropertySelect}
                                        onSearchChange={_.debounce(this.handlePropertySearchChange, 250, {
                                            leading: true,
                                        })}
                                        style={{ borderRadius: 0 }}
                                        minCharacters={0}
                                        results={this.state.newPropertySearchResults}
                                        value={this.state.newPropertyInput}
                                    />
                                </Table.Cell>
                                <Table.Cell>
                                    {
                                        this.state.newProperty &&
                                        <InputFlexibox
                                            prop={this.state.newProperty}
                                            setValue={this.setValueForProperty}
                                        >
                                        </InputFlexibox>
                                    }
                                </Table.Cell>
                                <Table.Cell>
                                    { this.state.newPropertyValue &&
                                        <Button
                                            primary
                                            content='Save'
                                            onClick={this.saveProperty}
                                        />
                                    }
                                    { this.state.newProperty &&
                                        <Button
                                            content='Clear'
                                            onClick={this.clearPropertyInputs}
                                        />
                                    }
                                </Table.Cell>
                            </Table.Row>
                        }
                        </Table.Body>
                    </Table>
                </Container>
            );
        }
    }
}

const mapStateToProps = (state: any, ownProps: any) => {
  return {
    ...state,
    ...ownProps,
  }
}

const mapDispatchToProps = dispatch => {
  return {
      refreshItem: (itemId: string) => dispatch({ type: 'REFRESH_ITEM', itemId: itemId }),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ItemPropsTable);