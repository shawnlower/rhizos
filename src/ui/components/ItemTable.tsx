import * as React from 'react';
import { Link } from 'react-router-dom';
import { connect } from "react-redux"
import _ from 'lodash'

import { Icon, Menu, Modal, Table, Button } from 'semantic-ui-react';
import { Item, ItemType } from '../../common/models';
import { DELETE_ITEM } from '../redux/actionTypes';
import ItemIcon from './ItemIcon';

/*
  Render a list of items

*/

export interface State {
  itemTypes: ItemType[]; // Cached list of types that we care about
}

export interface Props {
  filteredItems: Item[];
  deleteItem: any;
}

export interface ItemTypeMap {
  [typeName:string]: ItemType
}

class ItemTable extends React.Component<Props, State> {
  private initialState = {
    itemTypes: [],
  }
  constructor(props) {
    super(props);
    this.state = this.initialState;
  }

  render() {
    return (
      <Table celled>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Type</Table.HeaderCell>
            <Table.HeaderCell>Name</Table.HeaderCell>
            <Table.HeaderCell>Created</Table.HeaderCell>
            <Table.HeaderCell
              style={{ width: "100px" }}
            >Delete</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {
            this.props.filteredItems.map((item: Item) => {
              if (item) {
                return (
                  <Table.Row key={item.id}>
                    <Table.Cell>
                      <ItemIcon iconSize="large" itemTypeId={item.itemTypeId} />
                      <Link to={`/types/${item.itemTypeId}`}>{item.itemTypeId}</Link>
                    </Table.Cell>
                    <Table.Cell>
                      <Link to={`/items/${item.id}`}>{item.title}</Link>
                    </Table.Cell>
                    <Table.Cell>{item.dateCreated ? item.dateCreated.toLocaleString() : "-"}</Table.Cell>
                    <Table.Cell>
                      <Button
                        onClick={() => this.props.deleteItem(item.id)}
                      >Delete</Button>
                    </Table.Cell>
                  </Table.Row>
                );
              }
            })
          }
        </Table.Body>

        <Table.Footer>
          <Table.Row>
            <Table.HeaderCell colSpan='3'>
              <Menu floated='right' pagination>
                <Menu.Item as='a' icon>
                  <Icon name='chevron left' />
                </Menu.Item>
                <Menu.Item as='a'>1</Menu.Item>
                <Menu.Item as='a'>2</Menu.Item>
                <Menu.Item as='a'>3</Menu.Item>
                <Menu.Item as='a'>4</Menu.Item>
                <Menu.Item as='a' icon>
                  <Icon name='chevron right' />
                </Menu.Item>
              </Menu>
            </Table.HeaderCell>
          </Table.Row>
        </Table.Footer>
      </Table>
    );
  }

  componentDidMount () {
    let itemsToLookup: Item[] = [];
    this.props.filteredItems.map(item => {
      if (this.props.itemType.byId[item.itemTypeId] == undefined) {
        itemsToLookup.push(item);
      }
    });
    getTypesForItems(itemsToLookup).then(itemTypes =>
      this.setState({ itemTypes }));
  }
}

async function getTypesForItems(items: Item[]): Promise<ItemType[]> {
  /*
   * Take a list of items and return their associated types
   */
  return Promise.all(items.map(item => {
    return item.getItemType();
  }));
}

const mapStateToProps = (state: any, ownProps: any) => {
  return {
    ...state,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    //  completeTaskInstance: (taskInstance: TaskInstance) => dispatch({type: 'COMPLETE_TASK_INSTANCE', taskInstance}),
    deleteItem: (itemId: string) => dispatch({type: DELETE_ITEM, itemId}),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ItemTable);