import * as React from 'react';
import { Placeholder } from 'semantic-ui-react';

export interface Props {
    search: any; // redux method
    itemSearch: any; // redux store
}

export class InputPropertyCompleter extends React.Component<Props> {

    render () {
        return ( <Placeholder>InputPropertyCompleter</Placeholder>)
    }

}

const mapStateToProps = (state: any, ownProps: any) => {
    return {
        itemSearch: { ...state.itemSearch },
    }
}

const mapDispatchToProps = dispatch => {
    return {
        search: (query: string) => dispatch({ type: 'REFRESH_SEARCH_ITEMS', query: { query } }),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(InputPropertyCompleter);