import * as React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import {
    Image,
    Label,
    Message,
    Search,
    SearchResultData,
    Button,
} from 'semantic-ui-react'

import { ItemSearchReducerProps } from '../redux/reducers/itemSearch';
import { ItemSearchQuery } from '../services/itemApi';


/*
    Input Item Completer has two modes:

    1) Display Mode

        [ John Doe          ] (Edit)

        Shows a text summary of the item, with an Edit button to launch the
        type editor modal.

    2) Edit Mode
    
        [_G_________________] ( New )
        | Gladys Galactose  |
        | Gustov Grith      |
        ---------------------

        Shows an autocompleter of items matching the search text

    State needs to accomodate:
    - (Optional) Item Type to restrict searches to
    - Filtered results
    - Search String

*/

export interface State {
    value: string;
    mode: 'search' | 'display';
}
const initialState = {
    value: "",
    mode: 'search',
}

export interface Props {
    doSearch: any; // redux method to trigger search
    search: ItemSearchReducerProps; // redux store containing our state
    selectItem: any; // callback to receive item ID
    itemTypeId?: string;
}

const resultRenderer = (r: any) => (
    <>
        <Image src={r.icon} avatar /><span>{r.type}</span>
        <Label content={r.title} />
        <Label content={r.dateCreated.toLocaleDateString()} />
    </>
);

export class InputItemCompleter extends React.Component<Props> {

    constructor(props) {
        super(props);

        console.log("InputItemCompleter props", this.state, this.props)
        this.state = initialState;
        this.handleSearchChange = this.handleSearchChange.bind(this);
        this.handleResultSelect = this.handleResultSelect.bind(this);
    }

    handleResultSelect(e: any, { result }: SearchResultData) {
        const item = _.find(this.props.search.items, { id: result.id });
        this.setState({ value: item.title })
        if (!this.props.selectItem) {
            console.error('handleResultSelect no callback provided', result)
        } else {
            this.props.selectItem(item)
        }
    }

    handleSearchChange(e: any, { value }: { value: string }) {
        console.log('handleResultChange', value);
        const query: ItemSearchQuery = {
             ...this.props.search.query,
             query: value,
        }

        // Launch async query
        this.props.doSearch(query);
    }

    render () {
        const { isLoading, value, query, results } = this.props.search;

        return (
            <>
                <Search
                    fluid
                    loading={isLoading}
                    onResultSelect={this.handleResultSelect}
                    onSearchChange={this.handleSearchChange}
                    results={results}
                    value={this.state.value}
                    // resultRenderer={resultRenderer}
                >
            </Search>
            </>
            
        )
    }

    componentDidMount() {
        const itemTypeId = this.props.itemTypeId;
        this.props.doSearch({ ...this.props.search.query, itemTypeId: itemTypeId});
    }

}

const mapStateToProps = (state: any, ownProps: any) => {
    return {
        search: { ...state.itemSearch },
    }
}

const mapDispatchToProps = dispatch => {
    return {
        doSearch: (query: ItemSearchQuery) => dispatch({ type: 'REFRESH_SEARCH_ITEMS', query }),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(InputItemCompleter);