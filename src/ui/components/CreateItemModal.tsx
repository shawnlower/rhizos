import * as React from 'react';
import { connect } from "react-redux"
import { Button, Header, Image, Modal, Form, Input, TextArea, Select, Divider, Message, Placeholder } from 'semantic-ui-react'

import { ItemTypeReducerProps } from '../redux/reducers/itemType';

import { InputItemCompleter } from './InputItemCompleter';
import { Item, ItemProperty, IItemPropertyDTO } from '../../common/models';
import { T_Note, T_WebPage, T_Task } from '../../common/types';
import { ItemType } from '../../common/models';
import { render } from 'react-dom';

export interface Props {
    refreshTypes: any;
    itemType: ItemTypeReducerProps;
    item: Item | null; // Passed from parent or null to create a new item
}

// Move to models.ts
export interface NewItemRequest {
    name: string;
    itemTypeId: string;
    status: "clean" | "dirty";
    properties: { name: string; type: string; value: string; }[];
}

export interface State {
    newItemRequest?: NewItemRequest,
    itemTypes: ItemType[];
    isLoading: boolean;
}

export interface InputOptions {
    key: string;
    text: string;
    value: string;
}

const initialItemRequest: NewItemRequest = {
    name: "",
    itemTypeId: "Note",
    status: "dirty",
    properties: [],
}

const initialState: State = {
    itemTypes: [],
    isLoading: true,
    newItemRequest: initialItemRequest,
}

const DEFAULT_TYPE='Book';

export class CreateItemModal extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            ...initialState,
        }
        this.handleTypeChange = this.handleTypeChange.bind(this);
        this.setProp = this.setProp.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleSubmit() {
        console.log("Creating new item {type, item}", this.state.newItemRequest);
        const newItemRequest = this.state.newItemRequest;
        const itemType: ItemType | null = this.getTypeById(this.state.newItemRequest.itemTypeId);
        if (!itemType) {
            return new Error(`Unable to lookup itemType for ${newItemRequest}`);
        }

        const properties: IItemPropertyDTO[] = [];
        console.warn("unimplemented: not adding properties",
            this.state.newItemRequest.properties);

        const item = Item.create({
            name: newItemRequest.title,
            item_type: newItemRequest.itemTypeId,
            // date_created: newItemRequest.dateCreated || new Date(),
            id: newItemRequest.id || "",
            properties: properties,
        });
        console.log("Calling onCreateItem with item: ", item);
        this.props.onCreateItem(item);
    }

    getTypesAsOptions() {
        // Return itemTypes to be used in an <input options=...>
        if (Object.values(this.props.itemType.byId).length === 0) {
            console.warn("getTypesAsOptions: no item types loaded");
            return []
        }
        const itemTypes: InputOptions[] = Object.values(
            this.props.itemType.byId).map(
                (t: ItemType) => ({ key: t.id, text: t.name, value: t.name }));
        return itemTypes;
    }

    getTypeById(id: string): ItemType | null {
        return this.props.itemType.byId[id];
    }

    setProp(prop: ItemProperty, value: any) {
        /*
        Set a property in the item property map, which will be used to construct
        a new Item when the form is validated and submitted.
        */
        const { newItemRequest } = this.state;

        // Extend newItemRequest properties
        if (newItemRequest){
            newItemRequest.properties = {
                ...newItemRequest.properties,
                [prop.id]: value
            }
            this.setState({ newItemRequest });
        }
    }

    handleTypeChange(e: any, { value }: { value: string }) {
        // When the type changes, we need to lookup the type object in the map
        // value can be a string, boolean, number..
        const itemType: ItemType = this.props.itemType.byId[value];
        if (!itemType) {
            console.warn("handleTypeChange: Unable to lookup", value);
            return;
        }

        // Clear the existing new item, and update the type
        this.setState({
            newItemRequest: {
                ...initialItemRequest,
                itemTypeId: value,
            },
        });
    }

    handleChange(event: any) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            newItemRequest: { ...this.state.newItemRequest, [name]: value }
        });
    }

    isLoading(): boolean {
        // this.props.
        return this.props.itemType.isLoading; // || this.item.isLoading?
    }

    getPropertiesList(): ItemProperty[] {
        if (!this.state.newItemRequest) return [];
        const itemType = this.getTypeById(this.state.newItemRequest.itemTypeId);
        if (itemType && itemType.properties.length > 0) {
            return itemType.properties;
        } else {
            return [];
        }
    }

    itemFormGroup(prop: ItemProp) {
        /* Returns a Form Group containing:
         * <Form.Group>
         *   Person: [ ItemCompleter for Person ] [ open in new window]
         * </Form.Group>
         */
        return <Placeholder key={prop.id}>Item Form Group for {prop.name}</Placeholder>
    }

    render() {
        if (this.isLoading()) {
            return <Message>Loading...</Message>
        }
        return (
            <Modal
                trigger={<Button>Create Item</Button>}
                centered={false}
            >
                <Modal.Header>Create a new item</Modal.Header>
                <Modal.Content image>
                    <Form
                        loading={this.isLoading()}
                        onSubmit={this.handleSubmit}
                    >
                        <Form.Group widths='equal'>
                            {/*  Item Type */}
                            <Form.Field
                                control={Select}
                                options={this.getTypesAsOptions()}
                                label={{ children: 'Item Type', htmlFor: 'form-select-control-item-type' }}
                                search
                                searchInput={{ id: 'form-select-control-item-type' }}
                                onChange={this.handleTypeChange}
                                defaultValue={DEFAULT_TYPE}
                            />
                            <Form.Field
                                id='form-input-control-name'
                                control={Input}
                                label='Name'
                                required={true}
                                value={this.state.newItemRequest ? this.state.newItemRequest.name : ""}
                                name="name"
                                onChange={this.handleChange}
                            />
                        </Form.Group>
                        {
                            // ---------------------------
                            // Begin Dynamic Property List
                            // ---------------------------
                            this.getPropertiesList().map(prop => {
                                const ignoredProps = ['name']; // special case
                                if (ignoredProps.indexOf(prop.name) === 0) {
                                    return "";
                                }

                                switch (prop.datatype) {
                                    case 'string':
                                        return (
                                            <Form.Field
                                                key={prop.id}
                                                id={`form-input-control-${prop.id}`}
                                                control={Input}
                                                label={prop.name}
                                                placeholder={prop.name}
                                                onChange={(e: any, { value }: { value: string }) => this.setProp(prop, value)}
                                            />
                                        )
                                    case 'text':
                                        return (
                                            <Form.Field
                                                key={prop.id}
                                                id={`form-input-text-${prop.id}`}
                                                control={TextArea}
                                                label={prop.name}
                                                placeholder={prop.name}
                                                onChange={(e: any, { value }: { value: string }) => this.setProp(prop, value)}
                                            />
                                        )
                                    case 'boolean':
                                        return (
                                            <Form.Checkbox
                                                key={prop.id}
                                                id={`form-input-checkbox-${prop.id}`}
                                                label={p.name}
                                                onChange={(e, { checked }) => this.setProp(prop, checked)}
                                            />
                                        )
                                    case 'date':
                                        return (
                                            <Form.Field
                                                width={6}
                                                key={prop.id}
                                                control={Input}
                                                id={`form-input-checkbox-${prop.id}`}
                                                label={prop.name}
                                                placeholder={new Date().toLocaleDateString()}
                                                onChange={(e, { value }: { value: boolean }) => this.setProp(prop, value)}
                                            />
                                        )
                                    default:
                                        return this.itemFormGroup(prop);
                                }
                            })

                        }
                        <Divider></Divider>
                        <Form.Field
                            id='form-button-control-public'
                            control={Button}
                            content='Confirm'
                            label='Label with htmlFor'
                        />
                    </Form>
                </Modal.Content>
            </Modal>
        )
    }

    public componentDidMount() {
        if (Object.values(this.props.itemType.byId).length === 0) {
            this.props.refreshTypes();
        }
        setTimeout(() => {
            // Trigger a type change to load item type data
            this.handleTypeChange(null, { value: DEFAULT_TYPE });

        }, 500);
    }

}

const mapStateToProps = (state: any, ownProps: any) => {
    return {
        itemType: { ...state.itemType },
    }
}

const mapDispatchToProps = dispatch => {
    return {
        refreshTypes: (query: string) => dispatch({ type: 'REFRESH_TYPES', query: { query } }),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateItemModal);