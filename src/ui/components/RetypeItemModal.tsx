import * as React from 'react';
import { connect } from "react-redux"
import { Button, Header, Image, Modal, Form, Input, TextArea, Select, Divider, Message, Placeholder, Label } from 'semantic-ui-react'

import { Item, ItemType } from '../../common/models';
import { render } from 'react-dom';
import { ItemIcon } from './ItemIcon';

export interface Props {
    // Redux
    itemType: any;
    retypeItem: any;
    refreshTypes: any;

    // OwnProps
    item: Item | null;
}

export interface State {
    newType: ItemType|null;
}

export interface InputOptions {
    key: string;
    text: string;
    value: string;
}

const initialState = {
    newType: null
}

export class RetypeItemModal extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        console.log('constructor', props);
        this.state = initialState;
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleTypeChange = this.handleTypeChange.bind(this);
    }

    handleTypeChange(event: any, { value }) {
        try {
            const newType = this.props.itemType.byId[value];
            console.log('typeChange -> ', newType.name)
            this.setState({
                newType
            });
        } catch(e) {
            console.error(e);
        }
    }

    handleSubmit() {
        // Handle the submit action. Dispatch the retype request

        let newType: ItemType;

        if (!this.state.newType) {
            console.error("Failed. ")
        } else {
            newType = this.props.itemType.byId[this.state.newType.id]
            console.log(`Retyping ${this.props.item.title} -> ${newType.name}`);
            this.props.retypeItem(this.props.item, newType);
        }
    }

    getTypesAsOptions(): InputOptions[] {
        // Return itemTypes to be used in an <input options=...>
        if (Object.values(this.props.itemType.byId).length === 0) {
            console.warn("getTypesAsOptions: no item types loaded");
            return []
        }
        const itemTypes: ItemType[] = Object.values(this.props.itemType.byId);
        const itemTypeOptions: InputOptions[] = itemTypes.map((t: ItemType) =>
            ({ key: t.id, text: t.name, value: t.name }));
        return itemTypeOptions;
    }

    getCurrentType(): ItemType {
        // if (!this.props.item) {
        //     return "";
        // }
        const currentType = this.props.itemType.byId[this.props.item.itemTypeId];
        return currentType;
        // return currentType ? currentType.name : "";
    }

    render() {
        const currentType = this.getCurrentType();
        return (
            <Modal
                trigger={<Header>{currentType ? currentType.name : ""}</Header>}
                centered={false}
            >
                <Modal.Header>Change Item Type</Modal.Header>
                <Modal.Content image>
                    <Form
                        loading={false}
                        onSubmit={this.handleSubmit}
                    >
                        <Form.Group
                            style= {{ width: "350px" }}
                        >
                            {/*  Item Type */
                            currentType && <>
                                    <Form.Field
                                        style={{ flexGrow: "1" }}
                                    >
                                        <Header as="h2">
                                            <ItemIcon
                                                iconSize="large"
                                                itemTypeId={currentType.id}
                                            />{currentType.name}
                                        </Header>
                                    </Form.Field>
                                </>
                            }
                            <Form.Field
                                control={Select}
                                options={this.getTypesAsOptions()}
                                label={{ children: 'New Type', htmlFor: 'form-select-control-item-type' }}
                                search
                                searchInput={{ id: 'form-select-control-item-type' }}
                                onChange={this.handleTypeChange}
                            />
                        </Form.Group>
                        <Divider></Divider>
                        <Form.Field
                            id='form-button-control-public'
                            control={Button}
                            content='Confirm'
                            label='Label with htmlFor'
                        />
                    </Form>
                </Modal.Content>
            </Modal>
        )
    }
    componentDidMount() {
        if (Object.values(this.props.itemType.byId).length == 0) {
            console.log('RetypeItemModal: refreshing types');
            this.props.refreshTypes();
        }
    }

}

const mapStateToProps = (state: any, ownProps: any) => {
    return {
        itemType: { ...state.itemType },
        ...ownProps,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        retypeItem: (item: Item, newType: ItemType) => dispatch({ type: 'RETYPE_ITEM', item: item, newType: newType }),
        refreshTypes: (query: string) => dispatch({ type: 'REFRESH_TYPES', query: { query } }),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RetypeItemModal);