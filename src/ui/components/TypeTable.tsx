import * as React from 'react';
import { Link } from 'react-router-dom';
import { connect } from "react-redux"
import _ from 'lodash'

import { Icon, Menu, Modal, Table } from 'semantic-ui-react';
import { Item, ItemType } from '../../common/models';
import { resolve } from 'url';
import { ItemIcon } from './ItemIcon';

/*
  Render a list of item types

*/

export interface State {
}

export interface Props {
  itemType: any,
}

class TypeTable extends React.Component<Props, State> {

  render() {
    return (
      <Table celled>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Type</Table.HeaderCell>
            <Table.HeaderCell>Description</Table.HeaderCell>
            <Table.HeaderCell>Parent</Table.HeaderCell>
            <Table.HeaderCell>Number of Properties</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {
            Object.values(_.sortBy(this.props.itemType.byId, [ 'name', 'properties' ]))
              .map((itemType: ItemType) => {
                // console.log(Object.values(this.props.itemType.byId))
              return (
                <Table.Row key={itemType.id}>
                  <Table.Cell>
                    <ItemIcon iconSize="large" itemTypeId={itemType.id} />
                    <Link to={`/types/${itemType.id}`}>{itemType.name}</Link>
                  </Table.Cell>
                  <Table.Cell>
                  {itemType.description}
                  </Table.Cell>
                  <Table.Cell>-</Table.Cell>
                  <Table.Cell>
                  {itemType.properties.length}
                  </Table.Cell>
                </Table.Row>
              )
            })
          }
        </Table.Body>

        <Table.Footer>
          <Table.Row>
            <Table.HeaderCell colSpan='3'>
              <Menu floated='right' pagination>
                <Menu.Item as='a' icon>
                  <Icon name='chevron left' />
                </Menu.Item>
                <Menu.Item as='a'>1</Menu.Item>
                <Menu.Item as='a'>2</Menu.Item>
                <Menu.Item as='a'>3</Menu.Item>
                <Menu.Item as='a'>4</Menu.Item>
                <Menu.Item as='a' icon>
                  <Icon name='chevron right' />
                </Menu.Item>
              </Menu>
            </Table.HeaderCell>
          </Table.Row>
        </Table.Footer>
      </Table>
    );
  }

  componentDidMount () {
  }
}

const mapStateToProps = (state: any, ownProps: any) => {
  return {
    itemType: state.itemType,

  }
}

const mapDispatchToProps = dispatch => {
  return {
    //  completeTaskInstance: (taskInstance: TaskInstance) => dispatch({type: 'COMPLETE_TASK_INSTANCE', taskInstance}),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(TypeTable);