import * as React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import {
    Input,
    Placeholder,
} from 'semantic-ui-react'

import InputItemCompleter from './InputItemCompleter';

import { ItemProperty } from '../../common/models';

/*
    Flexible input box

    A component that is passed a property, and based on the type that is
    valid as a value for the property (i.e. it's "range"), render an
    appropriate input.

    Pass a property for 'author' which expects to auto-complete 'Person's
        <InputFlexibox prop={author} />

    Pass a property for 'name' which just allows string input.
        <InputFlexibox prop={name} />

    Pass a property for 'date' which shows a date selector
        <InputFlexibox prop={date} />

    Pass a property for 'item' which auto-completes any base item
        <InputFlexibox prop={item} />

*/

export interface State {
    value?: any;
}

const initialState = {
    value: ""
}

export interface Props {
    // doSearch: any; // redux method to trigger search
    prop: ItemProperty;
    setValue: any;
}


export class InputFlexibox extends React.Component<Props, State> {

    constructor(props) {
        super(props);
        this.state = initialState;

        this.doSelect = this.doSelect.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        const value = event.target.value;
        this.setState({ value });
        this.props.setValue(value); // pass the value back on every change
    }

    render () {
        const { datatype } = this.props.prop;

        switch(datatype) {
            case 'string':
                return (
                    <Input
                        onChange={this.handleChange}
                        value={this.state.value}
                    ></Input>
                )
                break;

            // case 'Person':
            //     return <InputItemCompleter itemTypeId={datatype} selectItem={this.doSelect}></InputItemCompleter>

            case "":
            case null:
            case undefined:
                return <Placeholder>Error! No datatype specified</Placeholder>
                break;

            default:
                return <InputItemCompleter itemTypeId={datatype} selectItem={this.doSelect}></InputItemCompleter>

        }
    }

    doSelect(v: any) {
        console.log('Selected!', v, this.props.prop);
        this.props.setValue(v);
    }

    componentDidMount() {
    }

}

const mapStateToProps = (state: any, ownProps: any) => {
    return {
        ...state,
        ...ownProps,
    }
}

const mapDispatchToProps = dispatch => {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(InputFlexibox);