import * as React from 'react';
import _ from 'lodash';
import { connect } from "react-redux"
import {  Icon, ItemDescription, Loader, SemanticICONS, SemanticSIZES } from 'semantic-ui-react';


import { Item, ItemType } from '../../common/models';
import { ItemApi } from '../services/itemApi';

import { REFRESH_TYPES } from '../redux/actionTypes';

export interface State {
    iconSize: string;
}

export interface Props {
    // Redux
    // If store not provided, then can just look up item id prop against static list
    refreshTypes?: any; 
    itemType?: any;

    // ownProps
    itemTypeId: string;
    iconSize: SemanticSIZES;
}


export class ItemIcon extends React.Component<Props, State> {

    defaultIcon: SemanticICONS = "file alternate outline";

    private initialState = {
        iconSize: this.props.iconSize ? this.props.iconSize : "huge",
    }
    
    private STATIC_ICONS: {[key: string]: SemanticICONS} = {
        'Account': 'credit card outline',
        'Bar': 'beer',
        'Cafe': 'coffee',
        'Book': 'book',
        'Event': 'calendar check outline',
        'House': 'home',
        'Image': 'file image outline',
        'Movie': 'film',
        'Note': 'edit outline',
        'Person': 'address book',
        'Photo': 'photo',
        'Store': 'shopping cart',
        'Task': 'tasks',
        'Thing': 'file outline',
        'WebPage': 'firefox',
    }

    constructor(props: Props) {
        super(props);
        this.state = this.initialState;
    }

    render() {
        // If we don't have our itemType object yet, launch an async request
        // for it, and render the default
        if (!this.props.itemTypeId) {
            console.warn('ItemIcon: no item in props')
        }

        const itemTypeId = this.props.itemTypeId

        let icon: SemanticICONS = this.STATIC_ICONS[itemTypeId] || this.defaultIcon;

        if (this.props.itemType){
            const t = this.props.itemType.byId[itemTypeId];
            if(t && t.icon){
                icon = t.icon
            }
        }

        return <Icon
                name={icon}
                size={this.state.iconSize}
                />
    }

    componentDidMount() {
        try {
            // if(!this.props.itemType){
            //     console.warn("ItemIcon: CDU: No itemType passed", this.props)
            //     return;
            // }
            // if (!this.props.itemType.byId[this.props.itemTypeId]) {
            //     console.log('itemIcon: Fetching item types', this.props.itemType);
            //     // this.api.getTypeById(this.props.itemTypeId).then(itemType => {
            //     //     this.setState({ itemType });
            //     // })
            //     this.props.refreshTypes();
            // }
        } catch(e) {
            console.warn('caught error in icon', e);
        }
    }
}

const mapStateToProps = (state: any, ownProps: any) => {
    return {
        itemType: state.itemType,
        itemTypeId: ownProps.itemTypeId,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        refreshTypes: (query: string) => dispatch({type: REFRESH_TYPES, query}),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemIcon);