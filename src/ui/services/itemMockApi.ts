import { mockItems, mockTypes } from "../../common/mockData";

export class MockItemApi {
    constructor() {
        console.log(`Instantiated new mock API`);
    }

    getItemById(id: string): Promise<Item> {
        // console.log("getItemById API called with", id);
        return new Promise((resolve, reject) =>
            setTimeout((id: string) => {
                const itemDTO: IItemDTO | null | undefined = _.find(mockItems, { id });
                if (!itemDTO) {
                    reject(`Unable to find item: ${id}`);
                } else {
                    const item = Item.create(itemDTO, this);
                    resolve(item);
                }
            }, 100));
    }

    getItems(query: string): Promise<Item[]> {
        // console.log("getItems API called with", query, mockItems);
        return new Promise((resolve, reject) => {
            setTimeout(query => {
                const re = new RegExp(_.escapeRegExp(query), 'i')
                const isMatch = (item: IItemDTO) => re.test(item.title)
                const results: IItemDTO[] = mockItems.filter(isMatch);

                const itemDTOs = results.map(itemDTO => Item.create(itemDTO, this));
                resolve(itemDTOs);
            }, 100);
        })
    }

    getItemTypes(query: string): Promise<ItemType[]> {
        console.log("getItemTypes API called with", query, mockTypes);
        return new Promise((resolve, reject) => {
            setTimeout(query => {
                const re = new RegExp(_.escapeRegExp(query), 'i')
                const isMatch = (itemType: IItemTypeDTO) => re.test(itemType.name)
                const results: IItemTypeDTO[] = mockTypes.filter(isMatch);

                const itemTypeDTOs = results.map(itemTypeDTO => ItemType.create(itemTypeDTO));
                resolve(itemTypeDTOs);
            }, 100);
        })
    }

    getTypeById(id: string): Promise<ItemType> {
        if (!id) throw new Error("getTypeById: no id passed")

        // console.log("getItemTypeById API called with", id, mockTypes);
        return new Promise((resolve, reject) =>
            setTimeout(() => {
                const typeDTO: IItemTypeDTO | null | undefined = _.find(mockTypes, { id });
                if (!typeDTO) {
                    reject(`getTypeById: Unable to find item type: ${id}`);
                } else {
                    const itemType = ItemType.create(typeDTO);
                    resolve(itemType);
                }
            }, 100));
    }
}
