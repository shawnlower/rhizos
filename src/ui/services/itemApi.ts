import _ from 'lodash'
import faker from 'faker'

import { ItemSearchResult, Item, ItemProperty, ItemType } from "../../common/models";
import { IItemDTO, IItemTypeDTO, IItemPropertyDTO } from "../../common/models";
import { AssertionError } from 'assert';
import { NoEmitOnErrorsPlugin } from 'webpack';

export interface ItemSearchQuery {
    itemId?: string;
    itemTypeId?: string;
    query?: string;
}

export class ItemApi {
    API_BASE = '/api/v1';
    config = {}        

    constructor() {
        const url = `${this.API_BASE}/config`
        fetch(url, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then(async response => {
            this.config = await response.json();
            console.log(`Instantiated new API with: ${this.API_BASE}. Config: `, this.config);
        });

    }

    public async retypeItem(item: Item, newType: ItemType) {

        console.log(`API: Retyping item ${item.id} -> ${newType.id}`);

        const params: URLSearchParams = new URLSearchParams();
        params.append('new_type_id', newType.id);
        const url = `${this.API_BASE}/retype/${item.id}?${params.toString()}`;

        const response = await fetch(url, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        });

        let responseJson: { errors: string[] };
        let data = "";

        try {
            data = await response.text();
            responseJson = JSON.parse(data);
        } catch(e) {
            console.error("Unable to parse JSON in response", data, e)
            throw(e);
        }

        if (response.ok && responseJson) {
            return;
        }
        console.log("Request", body);
        throw new Error("Server error: " + data);
    }

    public async addItemProperty(item: Item, property: string, value: any) {

        console.log("Adding property", value)
        const changes = [
            { op: "add", property_id: property, value },
        ]
        this.updateItem(item, changes);
    }

    public async deleteItemProperty(item: Item, property: string) {

        const changes = [
            { op: "delete", property_id: property, value: "" },
        ]
        this.updateItem(item, changes);

    }

    public async updateItem(item: Item, changes: any) {
        // ItemChangeRequest
        // const changes = [
        //     { op: "add", value: { }},
        //     { op: "remove", value: { }},
        //     { op: "change", value: { }},
        // ]
        console.log('updateItem', item, changes)

        const url = `${this.API_BASE}/items/${item.id}`;

        const body = JSON.stringify({ changes: changes })
        const response = await fetch(url, {
            method: 'PATCH',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body,
        });

        let responseJson: { errors: string[] };
        let data = "";

        try {
            data = await response.text();
            responseJson = JSON.parse(data);
        } catch(e) {
            console.error("Unable to parse JSON in response", data, e)
            throw(e);
        }

        if (response.ok && responseJson) {
            return;
        }
        console.log("Request", body);
        throw new Error("Server error: " + data);
    }

    public async createItem(item: Item): Item {
        /*
        createItem: Create a new item in the remote store

        Will return the item with the id field populated.

        API:
            {
                "item_type": "Person",
                "name": "MacGyver",
                "properties": [  ]
            }

        Return:
            201 Created  : On successful creation

        Note: Will not return a 409 Conflict if an item already exists
              with the same name and type.
        */

        console.log("createItem API called with", item);
        if (!item) {
            throw new Error("item null or undefined");
        }

        const url = `${this.API_BASE}/items`;
        console.log('posting DTO', item.toDto())
        const response = await fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(item.toDto())
        });

        let responseJson: { item: IItemDTO };
        let data = "";

        try {
            data = await response.text();
            responseJson = JSON.parse(data);
        } catch(e) {
            console.error("Unable to parse JSON in response", data, e)
            throw(e);
        }

        if (response.ok && responseJson) {
            return Item.create(responseJson['item'])
        }
        throw new Error("Server error: " + data)
    }

    public async deleteItem(itemId: string) {
        /*
        deleteItem: Delete an existing item by its ID
        
        API:

        Return:
            200 OK  : On successful creation

        */

        console.log("deleteItem API called with", itemId);
        if (!itemId) {
            throw new Error("Item ID null or undefined");
        }

        const url = `${this.API_BASE}/items/${itemId}`;
        const response = await fetch(url, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        });

        let data = "";
        let errors: any;

        try {
            data = await response.text();
            errors = JSON.parse(data);
        } catch(e) {
            console.error("Unable to parse JSON in response", data, e)
            throw(e);
        }

        if (!response.ok) {
            throw new Error(`Server error: ${response.status}` + errors)
        }
    }

    public getItem(itemId: string): Promise<Item> {
        console.log("getItem API called with", itemId);
        if (!itemId) {
            throw new Error("getItem API: No itemId specified");
        }

        const url = `${this.API_BASE}/items/${itemId}`;
        return fetch(url, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        })
            .then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error("getItems() Server Error: " + response);
            })
            .then(json => {
                if (!json || !json.data) {
                    throw new Error("getItems(): Invalid JSON in response");
                }
                const item = Item.create(json.data, this);
                return item;
            });
    }

    public getItems(searchQuery: ItemSearchQuery = {}): Promise<Item[]> {
        // console.log("getItems API called with", searchQuery);

        let url: string;

        const params: URLSearchParams = new URLSearchParams();
        const queryMap = {
            itemId: "item_id",
            itemTypeId: "item_type_id",
            query: "query",
        }

        if (searchQuery.itemId) {
            // A single itemId is specified, make a specific request for it
            // Return value is a list of one item
            return this.getItem(searchQuery.itemId).then(item => [item]);
        } else if (Object.values(searchQuery).length == 0){
            // No query params passed, just issue query
            url = `${this.API_BASE}/items/`;
        } else {
            // Build query from search params

            for (const k of Object.keys(searchQuery)) {
                if(searchQuery[k]) { // Skip null/undefined
                    params.append(queryMap[k], searchQuery[k]);
                }
            }
            url = `${this.API_BASE}/items/?${params.toString()}`;
        }

        return fetch(url, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        })
            .then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error("getItems() Server Error", response);
            })
            .then(itemDTOs => {
                if (!itemDTOs.data.map) {
                    console.warn("Received 0 items from query: ", searchQuery, itemDTOs);
                }
                return itemDTOs.data.map(
                    (dto: IItemDTO) => {
                        return Item.create(dto, this);
                    })
            })
    }

    public getTypeById(id: string = ""): Promise<ItemType> {
        // console.log("getTypeById API called with", id);
        const url = `${this.API_BASE}/types/${id}?all_properties=true`;
        return fetch(url, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            })
            .then(response => response.json())
            .then(json => {
                const merged = {
                    ...json.metadata,
                    properties: json.properties,
                }
                // console.log("Creating ItemType from", json);
                return ItemType.create(merged)
            });
    }

    getItemTypes(query: string = ""): Promise<ItemType[]> {

        const url = `${this.API_BASE}/types/?all_properties=True`;

        console.log("getItemTypes API called with", query, url);

        return fetch(url, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        })
            .then(response => response.json().then(itemTypeDTOs => {
                console.log("getItemTypes JSON", itemTypeDTOs);
                return itemTypeDTOs.data.map(
                    (dto: IItemTypeDTO) => ItemType.create(dto))
            })).catch(err => {
                console.error("getItemTypes: Error in fetch: ", err)
                return [];
            });
    }
}