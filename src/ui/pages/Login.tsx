import React from 'react';
import { connect } from 'react-redux';
import { Container, Header, Select, Button, Divider, Menu, Input, Dropdown, Form, Segment, Label } from 'semantic-ui-react';

import { REFRESH_SEARCH_ITEMS, CREATE_ITEM } from '../redux/actionTypes';
import { ItemSearchReducerProps } from '../redux/reducers/itemSearch';
import { ItemSearchResult, Item, IItemDTO } from '../../common/models';


export interface Props {
    user: any;
}

export interface State {
}

export class LoginPage extends React.Component<Props, State> {

  render() {
    return (
      <Container>
        <Header>Please log in to continue</Header>
        <Segment>
          <Form>
            <Form.Input label='Enter Username' autoComplete="username" />
            <Form.Input label='Enter Password' autoComplete="current-password" type='password' />
            <Button>Log In</Button>
          </Form>
        </Segment>
      </Container>
    )
  }

}

const mapStateToProps = (state: any, ownProps: any) => {
  return {
    users: {...state.users},
  }
}

const mapDispatchToProps = dispatch => {
  return {
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);