import React from 'react';
import { connect } from 'react-redux';
import { Container, Header, Select, Button, Divider, Menu, Input, Dropdown } from 'semantic-ui-react';

import ItemTable from "../components/ItemTable";
import CreateItemModal from "../components/CreateItemModal";

import { REFRESH_SEARCH_ITEMS, CREATE_ITEM } from '../redux/actionTypes';
import { ItemSearchReducerProps } from '../redux/reducers/itemSearch';
import { ItemSearchResult, Item, IItemDTO } from '../../common/models';


export interface Props {
  search: ItemSearchReducerProps,
  refreshItems: any;
  createItem: any;
}

export interface State {
  quickCreateInput: string;
  quickCreateTypeId: string;
}

export class Items extends React.Component<Props, State> {

  inputQuickCreate = null; // React ref to input control

  onCreateItem(item: Item) {
    console.log("Creating Item", item);
  }

  quickCreateOptions = [
    { key: 'thing', text: 'Thing', value: 'Thing' },
    { key: 'task', text: 'Task', value: 'Task' },
    { key: 'note', text: 'Note', value: 'Note' },
    { key: 'event', text: 'Event', value: 'Event' },
  ]

  constructor(props: Props) {
    super(props);
    this.state = {
      quickCreateInput: "",
      quickCreateTypeId: "Thing"
    }

    this.inputQuickCreate = React.createRef();
    this.handleQuickCreateChange = this.handleQuickCreateChange.bind(this);
    this.handleQuickCreateSubmit = this.handleQuickCreateSubmit.bind(this);
  }

  handleQuickCreateChange(e, {id, value}) {
    // Handle changes to the quick create dropdown or input

    if(id === 'quick-create-input') {
      this.setState({quickCreateInput: value})
    } else if(id === 'quick-create-dropdown') {
      this.setState({quickCreateTypeId: value})
    } else {
      console.warn('handleQuickCreateChange triggered from unknown control', id, value, e)
    }
  }

  handleQuickCreateSubmit() {
    console.log("quickCreateSubmit called",
      this.state.quickCreateInput,
      this.state.quickCreateTypeId);

    const item = new Item(
      this.state.quickCreateInput,
      this.state.quickCreateTypeId,
      new Date(),
      null,
      [],
    );
    /*
     * Dispatch a new action to create the item
     */
    this.props.createItem(item);

  }

  public render() {

    return (<>
      <Container>
        <Menu borderless fluid>
          <Menu.Item
          >
            <Input
              type='text'
              id='quick-create-input'
              placeholder='Name of new...'
              action
              onChange={this.handleQuickCreateChange}
            >
              <input
                ref={this.inputQuickCreate}
                style={{ minWidth: "500px" }}
              />
              <Select
                id='quick-create-dropdown'
                onChange={this.handleQuickCreateChange}
                compact
                value={this.state.quickCreateTypeId}
                options={this.quickCreateOptions}
              />
              <Button
                type='submit'
                onClick={this.handleQuickCreateSubmit}
                disabled={this.state.quickCreateInput.length < 3}
              >Quick Create</Button>
            </Input>
          </Menu.Item>
          <Menu.Item position='right'>
            <CreateItemModal
              onCreateItem={(item: Item) => this.onCreateItem(item)}
            ></CreateItemModal>
          </Menu.Item>
        </Menu>
        <Divider />
        <ItemTable filteredItems={this.props.search.items}></ItemTable>
      </Container>
    </>
    )
  }

  public componentDidMount() {
    this.props.refreshItems();
    this.inputQuickCreate.current.focus();
  }
}

const mapStateToProps = (state: any, ownProps: any) => {
  return {
    search: state.itemSearch,
    ...state,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    refreshItems: (query: string) => dispatch({type: REFRESH_SEARCH_ITEMS, query}),
    createItem: (item: Item) => dispatch({type: CREATE_ITEM, item}),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Items);