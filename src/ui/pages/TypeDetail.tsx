import * as React from 'react';
import _ from 'lodash';

import { withRouter } from 'react-router-dom'
import { connect } from "react-redux"
import { Header, Message, Segment, List, } from 'semantic-ui-react';

import { Item, ItemType } from '../../common/models';
import { InputPropertyCompleter } from '../components/InputPropertyCompleter';

import { ItemApi } from '../services/itemApi';

export interface State {
}

export interface Props {
  match: any // route match
  refreshTypes: any; // refresh current type
  itemType: any;
}

class ItemDetail extends React.Component<Props, State> {

  constructor(props) {
    super(props);

    const { typeId } = this.props.match.params;
    this.state = {
      typeId, 
    }
  }

  render() {
    if (this.props.itemType.isLoading 	
	
	
      || this.getCurrentType() == null) {
      return <Message>Loading...</Message>
    }
    else {
      return (
        <Segment>
          <Header></Header>
          <List>
            {
              this.getCurrentType().properties.map(property => {
                return (
                  <List.Item key={property.id}>
                    <List.Icon name='alarm' />
                    <List.Content>
                      <List.Header>
                        {property.name}
                      </List.Header>
                      <List.Description>
                        A property of type {property.datatype}
                      </List.Description>
                    </List.Content>
                  </List.Item>
              )
              })
            }
          </List>
        </Segment>
      );
    }
  }

  getCurrentType(): ItemType {
    const { typeId } = this.state;
    const currentType = this.props.itemType.byId[typeId];
    console.log("currentType", currentType );
    return currentType;
  }
  componentDidMount() {
    if (!(Object.values(this.props.itemType.byId).length > 0)) {
      this.props.refreshTypes();
    }


    console.log('_', _);
    const api = new ItemApi();
    api.getItemTypes().then(results => console.log('results', results));
  }
}

const mapStateToProps = (state: any, ownProps: any) => {
  return {
    itemType: state.itemType,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    refreshTypes: (query?: string) => dispatch({type: 'REFRESH_TYPES', query: query}),
  }
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ItemDetail));