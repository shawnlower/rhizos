import React from 'react';
import { connect } from 'react-redux';
import { Container, Header, Segment, Button, Divider } from 'semantic-ui-react';

import TypeTable from "../components/TypeTable";
import CreateItemModal from "../components/CreateItemModal";

import { REFRESH_SEARCH_ITEMS } from '../redux/actionTypes';
import { ItemSearchReducerProps } from '../redux/reducers/itemSearch';
import { ItemSearchResult, Item } from '../../common/models';


export interface Props {
  search: ItemSearchReducerProps,
  refreshItems: any;
  // selectedItem?
}

export interface State {
}

export class TypeList extends React.Component<Props, State> {

  onCreateItem(item: Item) {
    console.log("Creating Item", item);
  }

  constructor(props: Props) {
    super(props);
    this.state = {
      showItemModal: false,
    }
  }

  public render() {
    return (<>
      <Container>
        <Header>Item Types</Header>
        <CreateItemModal
          onCreateItem={(item: Item) => this.onCreateItem(item)}
        ></CreateItemModal>
        <Divider></Divider>
        <TypeTable filteredItems={this.props.search.items}></TypeTable>
      </Container>
    </>
    )
  }

  public componentDidMount() {
    this.props.refreshItems();
  }
}

const mapStateToProps = (state: any, ownProps: any) => {
  return {
    search: state.itemSearch,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    refreshItems: (query: string) => dispatch({type: REFRESH_SEARCH_ITEMS, query}),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(TypeList);