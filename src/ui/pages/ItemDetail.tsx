import * as React from 'react';
import { withRouter } from 'react-router-dom'
import { connect } from "react-redux"
import { Button, Header, Icon, Message, Segment, Input, Divider, Placeholder, Menu, } from 'semantic-ui-react';

import { ItemDetailReducerProps } from '../redux/reducers/itemDetail';
import { Item, ItemType } from '../../common/models';
import ItemPropsTable from '../components/ItemPropsTable';
import ItemIcon from '../components/ItemIcon';
import RetypeItemModal from '../components/RetypeItemModal';

interface Props extends ItemDetailReducerProps {
  refreshItem: any;
  match: any; // router
}

class ItemDetail extends React.Component<Props> {

  handleMenuClick(e, v) {
    // console.log('handleMenuClick with', e, v);

    switch(v.id) {
      case "add-text-segment":
        /*
        Add a new text card to the content section.

        The following steps are needed:
          1) Create a new item of type 'Note'
          2) Create a new relationship between our item and the note.
          3) Add the note to the list of items in the content pane.

        */

        // Look up the note type:


        console.log('Adding text box');
        break;

      case "add-item-card":
        /*
        Add a new card to the content section representing an existing item.

        The following steps are needed:
          1) Present a search modal for the item.
            - Allow search by name and filter by type
            - Determine the card type to use
              - ItemPropsTable: Show the item name and a subset of props
              - Built-in card, Contact Card, Image card, Event card, etc
          2) Create a new relationship between our item and the note.
          3) Add the card to the list of items in the content pane.

        */

        console.log('Adding item card');
        break;

      case "add-item-table":
        /*
        Add a new card representing a list of items, sharing some property,
        generally that property is the type.

        The following steps are needed:
          1) Present a dialog allowing selection of the type (or other
             property, if that will be supported)
          2) Add an ItemTable to the content pane, with a filtered list of
             items based on type, and relationship.

        */

        console.log('Adding item table');
        break;

      default:
        console.warn("Unknown menu item clicked", v)
    }
  }

  render() {
    if (this.props.isLoading) {
      return <Message>Loading...</Message>
    } else if (!this.props.currentItem) {
      console.log('**', this.props);
      return <Message><h1>Unable to find item</h1></Message>
    }
    else {
      const item = this.props.currentItem;
      return (
      <>
          <Segment>
            <div
              style={{
                display: "flex",
              }}
            >
              <div
                style={{
                  margin: "0.25em",
                  padding: "0.25em",
                  textAlign: "center",
                }}
              >
                <ItemIcon itemTypeId={item.itemTypeId}></ItemIcon>
                <RetypeItemModal
                  item={item}
                >{item.itemTypeId}</RetypeItemModal>
              </div>
          <div style={{ flexGrow: 1}}>
            <label >Name</label>
            <Input
              id="input-title"
              value={item.title}
              style={{
                margin: "1em",
                fontSize: "1.25em",
                padding: "0.25em",
                width: "500px"
              }}
            />
          </div>
          <div
            style={{
              margin: "1em",
            }}
          >
            <Button>Archive</Button>
            <Button>Open with...</Button>
            <Button>Delete</Button>
          </div>
          </div>

          <Divider></Divider>
          <ItemPropsTable item={item}></ItemPropsTable>
        </Segment>
          <Menu fluid widths="3">
            <Menu.Item
              onClick={this.handleMenuClick}
              id='add-text-segment'
              name="New Note"
            />
            <Menu.Item
              onClick={this.handleMenuClick}
              id='add-item-card'
              name="Link Existing Item"
            />
            <Menu.Item
              onClick={this.handleMenuClick}
              id='add-item-table'
              name="Add List of Items"
            />
          </Menu>
        <Segment
          style={{
            minHeight: "40em",
            backgroundColor: "aliceblue",
          }}
        >
          <Placeholder>Related items table</Placeholder>
          <Placeholder>Related item 1</Placeholder>
        </Segment>
      </>
    );
      }
  }

  // componentDidUpdate(prevProps: Props, prevState: State) {
  //   console.log('cdu', prevProps, prevState, this.props )
  // }

  componentDidMount() {
    const { itemId } = this.props.match.params;
    this.props.refreshItem(itemId);
  }
}

const mapStateToProps = (state: any, ownProps: any) => {
  return {
    ...state.itemDetail,
    ...state,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    refreshItem: (itemId: string) => dispatch({type: 'REFRESH_ITEM', itemId: itemId}),
  }
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ItemDetail));