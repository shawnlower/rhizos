/* Item Types reducer */

import { ItemType } from "../../../common/models";
import { ItemApi } from "../../services/itemApi";

import {
  RETYPE_ITEM,
  REFRESH_TYPES,
  REFRESH_TYPES_COMPLETE,
} from "../actionTypes";

export interface ItemTypeReducerProps {
    byId: {[id:string]: ItemType};    // A list of all items retrieved from the server.
    isLoading: boolean;
}
const initialState: ItemTypeReducerProps = {
  byId: {},
  isLoading: false,
}

const api = new ItemApi();

export default function (state = initialState, action: any) {
  switch (action.type) {
    case RETYPE_ITEM: {
      /*
       Issue an async request to the API and set isLoading to true.
       When the result is returned, we can report our status and update
       isLoading
      */

      api.retypeItem(action.item, action.newType).then(() => {
        action.asyncDispatch({ type: REFRESH_TYPES });
      });
      return {
        ...state,
        isLoading: true,
      }
    }
    case REFRESH_TYPES: {
      /*
       Issue an async request to the API and set isLoading to true.
       When the results are returned, we can report our status and update
       isLoading
      */

      console.log('API: Fetching types', action.query);
      api.getItemTypes(action.query).then((results: ItemType[]) => {
        action.asyncDispatch({ type: REFRESH_TYPES_COMPLETE, results: results });
      });
      return {
        ...state,
        isLoading: true,
      }
    }
    case REFRESH_TYPES_COMPLETE: {
        /*
        Issue an async request to the API and set isLoading to true.
        When the results are returned, we can report our status and update
        isLoading
        */
        // Convert list of types into dictionary 
        const byId = action.results.reduce((obj: any, itemType: ItemType) => {
            obj[itemType.id] = itemType
            return obj
        }, {});

        console.log('Fetched types', byId);

        return {
            ...state,
            isLoading: false,
            byId: byId,
        }
    }

    default:
      return state;
  }
}