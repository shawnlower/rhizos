/* Item reducer */

import { Item } from "../../../common/models";
import { ItemApi } from "../../services/itemApi";

import {
  REFRESH_ITEM,
  REFRESH_ITEM_COMPLETE,
} from "../actionTypes";

export interface ItemDetailReducerProps {
  // all: [],    // A list of all items retrieved from the server.
  currentItem: Item | null;
  isLoading: boolean;
}

const initialState: ItemDetailReducerProps = {
  // all: [],    // A list of all items retrieved from the server.
  currentItem: null,
  isLoading: true,
}

const api = new ItemApi();

export default function (state = initialState, action: any) {
  switch (action.type) {
    case REFRESH_ITEM: {
      /*
       Issue an async request to the API and set isLoading to true.
       When the results are returned, we can report our status and update
       isLoading
      */

      api.getItem(action.itemId).then((results: Item) => {
        action.asyncDispatch({ type: REFRESH_ITEM_COMPLETE, results: results });
      });
      return {
        ...state,
        isLoading: true,
      }
    }
    case REFRESH_ITEM_COMPLETE: {
      /*
       * We've loaded some Items. Store them as Items in our list
       * then update isLoading
       */
      const item: Item = action.results;
      return {
        ...state,
        isLoading: false,
        currentItem: item,
      }
    }

    default:
      return state;
  }
}