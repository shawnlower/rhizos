import { combineReducers } from "redux";

import itemSearch from "./itemSearch";
import itemDetail from "./itemDetail";
import itemType from "./itemType";
import user from "./user";

export default combineReducers({ itemSearch, itemDetail, itemType, user });