const initialState = {
  username: "",
  name: "",
  avatar: "",
  loggedIn: false,
}

export default function(state = initialState, action: any) {
  switch (action.type) {
    default:
      return state;
  }
}

