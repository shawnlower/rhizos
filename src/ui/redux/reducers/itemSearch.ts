import { Item } from "../../../common/models";
import { ItemApi } from "../../services/itemApi";
import { SearchResultProps } from 'semantic-ui-react'

import { ItemSearchQuery } from '../../services/itemApi';


/* Reducers specific to the task entry page */
import {
  REFRESH_SEARCH_ITEMS,
  REFRESH_SEARCH_ITEMS_COMPLETE,
  CREATE_ITEM,
  DELETE_ITEM,
} from "../actionTypes";
import { Items } from "../../pages/Items";

export interface ItemSearchReducerProps {
  query: ItemSearchQuery;
  results: SearchResultProps[];
  items: Item[];
  isLoading: boolean;
  value: string; // The actual search text
}

const initialState: ItemSearchReducerProps = {
  results: [],
  items: [],
  query: {},
  isLoading: false,
  value: "",
}

const api = new ItemApi();

const itemsToSearchResults = (items: Item[]): SearchResultProps[] => {
    // Converts a list of Items into a search result
  return items.map(item => ({
      id: item.id,
      title: item.title,
      description: item.title,
    }));
}

export default function(state = initialState, action: any) {
  switch (action.type) {
    case REFRESH_SEARCH_ITEMS: {
      /*
       Issue an async request to the API and set isLoading to true.
       When the results are returned, we can report our status and update
       isLoading
      */

      api.getItems(action.query).then((results: Item[]) => {
        action.asyncDispatch({ type: REFRESH_SEARCH_ITEMS_COMPLETE, results: results });
      });

      const value = action.query ? action.query.query : "";

      return {
        ...state,
        isLoading: true,
        value,
        query: action.query,
      }
    }

    case CREATE_ITEM: {
      /*
      Request the creation of an item
      */

      api.createItem(action.item).then((item: Item) => {
        console.log('Created item', item);
        action.asyncDispatch({ type: REFRESH_SEARCH_ITEMS });
      });

      return {
        ...state,
        isLoading: true,
      }
    }

    case DELETE_ITEM: {
      /*
      Request the deletion of an item
      */

      api.deleteItem(action.itemId).then(() => {
        console.log('Deleted item', action.itemId);
        action.asyncDispatch({ type: REFRESH_SEARCH_ITEMS });
      });

      return {
        ...state,
        isLoading: true,
      }
    }

    case REFRESH_SEARCH_ITEMS_COMPLETE: {
      /*
       * We've loaded some itemDTOs. Store them as Items in our list
       * then update isLoading
       */
      const items: Item[] = action.results;

      return {
        ...state,
        isLoading: false,
        results: itemsToSearchResults(items),
        items,
      }
      return {
        ...state,
      }
    }

    default:
      return state;
  }
}