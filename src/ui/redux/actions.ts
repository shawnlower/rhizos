import { REFRESH_SEARCH_ITEMS } from "./actionTypes";

export const refreshTasks = (content: any) => ({
  type: REFRESH_SEARCH_ITEMS,
  payload: {
    content
  }
});